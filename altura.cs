using System;

namespace _8_estatura
{
    class Estatura
    {
          static void Main(string[] args)
          {
         Estatura e = new Estatura();
               e.almacenar();
               e.obtener_promedio();
               e.mayor_menor();
          }

          private float[] altura;
          private float promedio;
          
          public void almacenar()
          {
               altura = new float[5];
               for (int v = 0; v < 5; v++)
               {
                    Console.Write("Introdusca la altura de las personas: \n");
                    string captador = Console.ReadLine();
                    altura[v] = float.Parse(captador);
               }
          }

          public void obtener_promedio()

          {
               float suma;
               suma = 0;
               for (int v = 0; v < 5; v++)
               {
                    suma = suma + altura[v];
               }
               promedio = suma / 5;
               Console.WriteLine("EL Promedio de Estatura es : " + promedio);
          }

          public void mayor_menor()
          {
              
               int mayor=0, menor=0;
               for (int v = 0; v < 5; v++)
               {
                    if (altura[v] > promedio)
                    {
                         mayor++;
                    }
                    else
                    {
                         if (altura[v] < promedio)
                         {
                              menor++;
                         }
                    }
               }
              
               Console.WriteLine("Las personas mayores al promedio son: " + mayor);
               Console.WriteLine("Las personas menores al promedio son: " + menor);
               Console.ReadKey();
          }

         
     }
    }