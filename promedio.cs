using System;

namespace ejerc.___1_media_aritmetica
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] valores= new double[4]; 
            double suma=0; 
            double promedio; 
            Console.WriteLine("Este programa almacena 4 valores introducidos por el usuario, y luego proyecta el promedio y los valores"); 
          
            for(int a=0; a<4; a++){
                Console.Write("Introduzca el valor # {0}: ", a+1); 
                valores[a] = Convert.ToDouble(Console.ReadLine()); 
                suma += valores[a]; 
            }
            promedio = suma / 4;
            Console.Write("\nLos valores introducidos son: "); 
            for(int a=0; a<4; a++){Console.Write(" ({0}) ", 
            valores [a]); 
            }
            Console.Write("\nY El promedio aritmetica es: {0}", promedio); 
        }
}
}