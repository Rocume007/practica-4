using System;

namespace numero
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numeros= new int[10]; 
            int a, mayor=0, c=0; 
            Console.WriteLine("Este programa almacena 10 numeros introducidos por el usuario, y luego proyecta el numero mayor "); 
            
            for(a=0; a<10; a++){
                Console.Write("Introduzca el valor # {0}: ", a+1); 
                numeros[a] = int.Parse(Console.ReadLine()); 
            } 
            while ( c <10)
            {
                if(numeros[c] > mayor)
                mayor = numeros[c];
                c++;
            }
             Console.Write("\n El numero mayor  es: {0}", mayor);

            Console.ReadKey();
        } 
    }
}

